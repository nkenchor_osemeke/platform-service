package io.angularpay.platform.helpers;

import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.*;
import io.angularpay.platform.exceptions.CommandException;
import io.angularpay.platform.exceptions.ErrorCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import static io.angularpay.platform.exceptions.ErrorCode.DUPLICATE_REQUEST_ERROR;
import static io.angularpay.platform.exceptions.ErrorCode.REQUEST_NOT_FOUND;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class CommandHelper {

    public static void validateServiceNotExistOrThrow(MongoAdapter mongoAdapter, String code) {
        mongoAdapter.findServiceByCode(code).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static Service getServiceByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findServiceByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static Service getServiceByCodeOrThrow(MongoAdapter mongoAdapter, String code) {
        return mongoAdapter.findServiceByCode(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateCurrencyNotExistOrThrow(MongoAdapter mongoAdapter, String code) {
        mongoAdapter.findCurrencyByCode(code).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static Currency getCurrencyByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findCurrencyByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static Currency getCurrencyByCodeOrThrow(MongoAdapter mongoAdapter, String code) {
        return mongoAdapter.findCurrencyByCode(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateCountryNotExistOrThrow(MongoAdapter mongoAdapter, String code) {
        mongoAdapter.findCountryByCode(code).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static Country getCountryByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findCountryByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static Country getCountryByCodeOrThrow(MongoAdapter mongoAdapter, String code) {
        return mongoAdapter.findCountryByCode(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static Country getCountryByIsoCode2OrThrow(MongoAdapter mongoAdapter, String code) {
        return mongoAdapter.findCountryByIsoCode2(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static Country getCountryByIsoCode3OrThrow(MongoAdapter mongoAdapter, String code) {
        return mongoAdapter.findCountryByIsoCode3(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateNotificationTypeNotExistOrThrow(MongoAdapter mongoAdapter, NotificationCode code) {
        mongoAdapter.findNotificationTypeByCode(code).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static NotificationType getNotificationTypeByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findNotificationTypeByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static NotificationType getNotificationTypeByCodeOrThrow(MongoAdapter mongoAdapter, NotificationCode code) {
        return mongoAdapter.findNotificationTypeByCode(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateOtpTypeNotExistOrThrow(MongoAdapter mongoAdapter, OtpCode code) {
        mongoAdapter.findOtpTypeByCode(code).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static OtpType getOtpTypeByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findOtpTypeByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static OtpType getOtpTypeByCodeOrThrow(MongoAdapter mongoAdapter, OtpCode code) {
        return mongoAdapter.findOtpTypeByCode(code).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateOTTLConfigurationNotExistOrThrow(MongoAdapter mongoAdapter) {
        mongoAdapter.getTTLConfiguration().ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static TTLConfiguration getTTLConfigurationOrThrow(MongoAdapter mongoAdapter) {
        return mongoAdapter.getTTLConfiguration().orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static MaturityConfiguration getMaturityConfigurationByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findMaturityConfigurationByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateMaturityConfigurationNotExistOrThrow(MongoAdapter mongoAdapter, String serviceCode) {
        mongoAdapter.findMaturityConfigurationByServiceCode(serviceCode).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    public static CountryFeature getCountryFeatureByReferenceOrThrow(MongoAdapter mongoAdapter, String reference) {
        return mongoAdapter.findCountryFeatureByReference(reference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
    }

    public static void validateCountryFeatureNotExistOrThrow(MongoAdapter mongoAdapter, String countryReference) {
        mongoAdapter.findCountryFeatureByCountryReference(countryReference).ifPresent(
                (x) -> {
                    throw commandException(HttpStatus.CONFLICT, DUPLICATE_REQUEST_ERROR);
                }
        );
    }

    private static CommandException commandException(HttpStatus status, ErrorCode errorCode) {
        return CommandException.builder()
                .status(status)
                .errorCode(errorCode)
                .message(errorCode.getDefaultMessage())
                .build();
    }

}
