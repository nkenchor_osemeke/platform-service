package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.Service;
import io.angularpay.platform.domain.commands.services.*;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.ServicesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/services")
@RequiredArgsConstructor
@Profile("!test")
public class ServicesRestApiAdapter implements ServicesRestApiPort {

    private final CreateServiceCommand createServiceCommand;
    private final UpdateServiceCommand updateServiceCommand;
    private final GetServiceByReferenceCommand getServiceByReferenceCommand;
    private final EnableServiceCommand enableServiceCommand;
    private final GetServiceByCodeCommand getServiceByCodeCommand;
    private final GetServiceListCommand getServiceListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericServiceApiModel genericServiceApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateServiceCommandRequest createServiceCommandRequest = CreateServiceCommandRequest.builder()
                .genericServiceApiModel(genericServiceApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createServiceCommand.execute(createServiceCommandRequest);
    }

    @PutMapping("/{serviceReference}")
    @Override
    public void update(
            @PathVariable String serviceReference,
            @RequestBody GenericServiceApiModel genericServiceApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateServiceCommandRequest updateServiceCommandRequest = UpdateServiceCommandRequest.builder()
                .reference(serviceReference)
                .genericServiceApiModel(genericServiceApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateServiceCommand.execute(updateServiceCommandRequest);
    }

    @GetMapping("/{serviceReference}")
    @ResponseBody
    @Override
    public Service getByReference(
            @PathVariable String serviceReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(serviceReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getServiceByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @PutMapping("/{resourceReference}/enable/{enable}")
    @Override
    public void enableService(
            @PathVariable String resourceReference,
            @PathVariable boolean enable,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        EnableServiceCommandRequest enableServiceCommandRequest = EnableServiceCommandRequest.builder()
                .reference(resourceReference)
                .enable(enable)
                .authenticatedUser(authenticatedUser)
                .build();
        this.enableServiceCommand.execute(enableServiceCommandRequest);
    }

    @GetMapping("/service-code/{code}")
    @ResponseBody
    @Override
    public Service getByCode(
            @PathVariable String code,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetServiceByCodeCommandRequest getServiceByCodeCommandRequest = GetServiceByCodeCommandRequest.builder()
                .code(code)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getServiceByCodeCommand.execute(getServiceByCodeCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<Service> listServices(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getServiceListCommand.execute(genericListCommandRequest);
    }

}
