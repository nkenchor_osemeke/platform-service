package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.MaturityConfiguration;
import io.angularpay.platform.domain.TTLConfiguration;
import io.angularpay.platform.domain.commands.maturity.CreateMaturityConfigurationCommand;
import io.angularpay.platform.domain.commands.maturity.GetMaturityConfigurationByReferenceCommand;
import io.angularpay.platform.domain.commands.maturity.GetMaturityConfigurationListCommand;
import io.angularpay.platform.domain.commands.maturity.UpdateMaturityConfigurationCommand;
import io.angularpay.platform.domain.commands.ttl.CreateTTLConfigurationCommand;
import io.angularpay.platform.domain.commands.ttl.GetTTLConfigurationCommand;
import io.angularpay.platform.domain.commands.ttl.UpdateTTLConfigurationCommand;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.ConfigurationRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/configurations")
@RequiredArgsConstructor
@Profile("!test")
public class ConfigurationsRestApiAdapter implements ConfigurationRestApiPort {

    private final CreateTTLConfigurationCommand createTTLConfigurationCommand;
    private final UpdateTTLConfigurationCommand updateTTLConfigurationCommand;
    private final GetTTLConfigurationCommand getTTLConfigurationCommand;

    private final CreateMaturityConfigurationCommand createMaturityConfigurationCommand;
    private final UpdateMaturityConfigurationCommand updateMaturityConfigurationCommand;
    private final GetMaturityConfigurationByReferenceCommand getMaturityConfigurationByReferenceCommand;
    private final GetMaturityConfigurationListCommand getMaturityConfigurationListCommand;

    @PostMapping("/ttl")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse createTTLConfiguration(
            @RequestBody GenericTTLApiModel genericTTLApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericTTLConfigurationCommandRequest genericTTLConfigurationCommandRequest = GenericTTLConfigurationCommandRequest.builder()
                .genericTTLApiModel(genericTTLApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createTTLConfigurationCommand.execute(genericTTLConfigurationCommandRequest);
    }

    @PutMapping("/ttl")
    @Override
    public void updateTTLConfiguration(
            @RequestBody GenericTTLApiModel genericTTLApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericTTLConfigurationCommandRequest updateOtpTypeCommandRequest = GenericTTLConfigurationCommandRequest.builder()
                .genericTTLApiModel(genericTTLApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateTTLConfigurationCommand.execute(updateOtpTypeCommandRequest);
    }

    @GetMapping("/ttl")
    @ResponseBody
    @Override
    public TTLConfiguration getTTLConfiguration(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericTTLCommandRequest genericTTLCommandRequest = GenericTTLCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getTTLConfigurationCommand.execute(genericTTLCommandRequest);
    }

    @PostMapping("/maturity")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse createMaturityConfiguration(
            @RequestBody GenericMaturityApiModel genericMaturityApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateMaturityConfigurationCommandRequest createMaturityConfigurationCommandRequest = CreateMaturityConfigurationCommandRequest.builder()
                .genericMaturityApiModel(genericMaturityApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createMaturityConfigurationCommand.execute(createMaturityConfigurationCommandRequest);
    }

    @PutMapping("/maturity/{maturityReference}")
    @Override
    public void updateMaturityConfiguration(
            @PathVariable String maturityReference,
            @RequestBody GenericMaturityApiModel genericMaturityApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateMaturityConfigurationCommandRequest updateMaturityConfigurationCommandRequest = UpdateMaturityConfigurationCommandRequest.builder()
                .reference(maturityReference)
                .genericMaturityApiModel(genericMaturityApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateMaturityConfigurationCommand.execute(updateMaturityConfigurationCommandRequest);
    }

    @GetMapping("/maturity/{maturityReference}")
    @ResponseBody
    @Override
    public MaturityConfiguration getMaturityConfigurationByReference(
            @PathVariable String maturityReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(maturityReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getMaturityConfigurationByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @GetMapping("/maturity")
    @ResponseBody
    @Override
    public List<MaturityConfiguration> listMaturityConfiguration(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getMaturityConfigurationListCommand.execute(genericListCommandRequest);
    }

}
