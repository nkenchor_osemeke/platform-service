package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.OtpCode;
import io.angularpay.platform.domain.OtpType;
import io.angularpay.platform.domain.commands.otp.*;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.OtpTypesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/otp-types")
@RequiredArgsConstructor
@Profile("!test")
public class OtpTypesRestApiAdapter implements OtpTypesRestApiPort {

    private final CreateOtpTypeCommand createOtpTypeCommand;
    private final UpdateOtpTypeCommand updateOtpTypeCommand;
    private final GetOtpTypeByReferenceCommand getOtpTypeByReferenceCommand;
    private final EnableOtpTypeCommand enableOtpTypeCommand;
    private final GetOtpTypeByCodeCommand getOtpTypeByCodeCommand;
    private final GetOtpTypeListCommand getOtpTypeListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericOtpTypeApiModel genericOtpTypeApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateOtpTypeCommandRequest createOtpTypeCommandRequest = CreateOtpTypeCommandRequest.builder()
                .genericOtpTypeApiModel(genericOtpTypeApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createOtpTypeCommand.execute(createOtpTypeCommandRequest);
    }

    @PutMapping("/{otpTypeReference}")
    @Override
    public void update(
            @PathVariable String otpTypeReference,
            @RequestBody GenericOtpTypeApiModel genericOtpTypeApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateOtpTypeCommandRequest updateOtpTypeCommandRequest = UpdateOtpTypeCommandRequest.builder()
                .reference(otpTypeReference)
                .genericOtpTypeApiModel(genericOtpTypeApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateOtpTypeCommand.execute(updateOtpTypeCommandRequest);
    }

    @GetMapping("/{otpTypeReference}")
    @ResponseBody
    @Override
    public OtpType getByReference(
            @PathVariable String otpTypeReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(otpTypeReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getOtpTypeByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @PutMapping("/{otpTypeReference}/enable/{enable}")
    @Override
    public void enableOtpType(
            @PathVariable String otpTypeReference,
            @PathVariable boolean enable,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        EnableOtpTypeCommandRequest enableOtpTypeCommandRequest = EnableOtpTypeCommandRequest.builder()
                .reference(otpTypeReference)
                .enable(enable)
                .authenticatedUser(authenticatedUser)
                .build();
        this.enableOtpTypeCommand.execute(enableOtpTypeCommandRequest);
    }

    @GetMapping("/otp-code/{code}")
    @ResponseBody
    @Override
    public OtpType getByCode(
            @PathVariable OtpCode code,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetOtpTypeByCodeCommandRequest getOtpTypeByCodeCommandRequest = GetOtpTypeByCodeCommandRequest.builder()
                .code(code)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getOtpTypeByCodeCommand.execute(getOtpTypeByCodeCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<OtpType> listOtpTypes(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getOtpTypeListCommand.execute(genericListCommandRequest);
    }

}
