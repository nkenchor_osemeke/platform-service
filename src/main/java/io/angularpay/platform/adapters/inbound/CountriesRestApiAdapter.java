package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.Country;
import io.angularpay.platform.domain.commands.countries.*;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.CountriesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/countries")
@RequiredArgsConstructor
@Profile("!test")
public class CountriesRestApiAdapter implements CountriesRestApiPort {

    private final CreateCountryCommand createCountryCommand;
    private final UpdateCountryCommand updateCountryCommand;
    private final GetCountryByReferenceCommand getCountryByReferenceCommand;
    private final EnableCountryCommand enableCountryCommand;
    private final GetCountryByCodeCommand getCountryByCodeCommand;
    private final GetCountryByIsoCode2Command getCountryByIsoCode2Command;
    private final GetCountryByIsoCode3Command getCountryByIsoCode3Command;
    private final GetCountryListCommand getCountryListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericCountryApiModel genericCountryApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateCountryCommandRequest createCountryCommandRequest = CreateCountryCommandRequest.builder()
                .genericCountryApiModel(genericCountryApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createCountryCommand.execute(createCountryCommandRequest);
    }

    @PutMapping("/{countryReference}")
    @Override
    public void update(
            @PathVariable String countryReference,
            @RequestBody GenericCountryApiModel genericCountryApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateCountryCommandRequest updateCountryCommandRequest = UpdateCountryCommandRequest.builder()
                .reference(countryReference)
                .genericCountryApiModel(genericCountryApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateCountryCommand.execute(updateCountryCommandRequest);
    }

    @GetMapping("/{countryReference}")
    @ResponseBody
    @Override
    public Country getByReference(
            @PathVariable String countryReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(countryReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @PutMapping("/{countryReference}/enable/{enable}")
    @Override
    public void enableCountry(
            @PathVariable String countryReference,
            @PathVariable boolean enable,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        EnableCountryCommandRequest enableCountryCommandRequest = EnableCountryCommandRequest.builder()
                .reference(countryReference)
                .enable(enable)
                .authenticatedUser(authenticatedUser)
                .build();
        this.enableCountryCommand.execute(enableCountryCommandRequest);
    }

    @GetMapping("/country-code/{code}")
    @ResponseBody
    @Override
    public Country getByCode(
            @PathVariable String code,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetCountryByCodeCommandRequest getCountryByCodeCommandRequest = GetCountryByCodeCommandRequest.builder()
                .code(code)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryByCodeCommand.execute(getCountryByCodeCommandRequest);
    }

    @GetMapping("/iso-code-2/{isoCode2}")
    @ResponseBody
    @Override
    public Country getByIsoCode2(
            @PathVariable String isoCode2,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetCountryByCodeCommandRequest getCountryByCodeCommandRequest = GetCountryByCodeCommandRequest.builder()
                .code(isoCode2)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryByIsoCode2Command.execute(getCountryByCodeCommandRequest);
    }

    @GetMapping("/iso-code-3/{isoCode3}")
    @ResponseBody
    @Override
    public Country getByIsoCode3(
            @PathVariable String isoCode3,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetCountryByCodeCommandRequest getCountryByCodeCommandRequest = GetCountryByCodeCommandRequest.builder()
                .code(isoCode3)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryByIsoCode3Command.execute(getCountryByCodeCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<Country> listCountries(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryListCommand.execute(genericListCommandRequest);
    }

}
