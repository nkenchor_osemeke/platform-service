package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.NotificationCode;
import io.angularpay.platform.domain.NotificationType;
import io.angularpay.platform.domain.commands.notifications.*;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.NotificationTypesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/notification-types")
@RequiredArgsConstructor
@Profile("!test")
public class NotificationTypesRestApiAdapter implements NotificationTypesRestApiPort {

    private final CreateNotificationTypeCommand createNotificationTypeCommand;
    private final UpdateNotificationTypeCommand updateNotificationTypeCommand;
    private final GetNotificationTypeByReferenceCommand getNotificationTypeByReferenceCommand;
    private final EnableNotificationTypeCommand enableNotificationTypeCommand;
    private final GetNotificationTypeByCodeCommand getNotificationTypeByCodeCommand;
    private final GetNotificationTypeListCommand getNotificationTypeListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericNotificationTypeApiModel genericNotificationTypeApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateNotificationTypeCommandRequest createNotificationTypeCommandRequest = CreateNotificationTypeCommandRequest.builder()
                .genericNotificationTypeApiModel(genericNotificationTypeApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createNotificationTypeCommand.execute(createNotificationTypeCommandRequest);
    }

    @PutMapping("/{notificationTypeReference}")
    @Override
    public void update(
            @PathVariable String notificationTypeReference,
            @RequestBody GenericNotificationTypeApiModel genericNotificationTypeApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateNotificationTypeCommandRequest updateNotificationTypeCommandRequest = UpdateNotificationTypeCommandRequest.builder()
                .reference(notificationTypeReference)
                .genericNotificationTypeApiModel(genericNotificationTypeApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateNotificationTypeCommand.execute(updateNotificationTypeCommandRequest);
    }

    @GetMapping("/{notificationTypeReference}")
    @ResponseBody
    @Override
    public NotificationType getByReference(
            @PathVariable String notificationTypeReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(notificationTypeReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getNotificationTypeByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @PutMapping("/{notificationTypeReference}/enable/{enable}")
    @Override
    public void enableNotificationType(
            @PathVariable String notificationTypeReference,
            @PathVariable boolean enable,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        EnableNotificationTypeCommandRequest enableNotificationTypeCommandRequest = EnableNotificationTypeCommandRequest.builder()
                .reference(notificationTypeReference)
                .enable(enable)
                .authenticatedUser(authenticatedUser)
                .build();
        this.enableNotificationTypeCommand.execute(enableNotificationTypeCommandRequest);
    }

    @GetMapping("/notification-code/{code}")
    @ResponseBody
    @Override
    public NotificationType getByCode(
            @PathVariable NotificationCode code,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetNotificationTypeByCodeCommandRequest getNotificationTypeByCodeCommandRequest = GetNotificationTypeByCodeCommandRequest.builder()
                .code(code)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getNotificationTypeByCodeCommand.execute(getNotificationTypeByCodeCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<NotificationType> listNotificationTypes(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getNotificationTypeListCommand.execute(genericListCommandRequest);
    }

}
