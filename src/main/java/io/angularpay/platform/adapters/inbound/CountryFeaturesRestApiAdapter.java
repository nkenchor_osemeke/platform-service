package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.CountryFeature;
import io.angularpay.platform.domain.commands.features.CreateCountryFeatureCommand;
import io.angularpay.platform.domain.commands.features.GetCountryFeatureByReferenceCommand;
import io.angularpay.platform.domain.commands.features.GetCountryFeatureListCommand;
import io.angularpay.platform.domain.commands.features.UpdateCountryFeatureCommand;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.CountryFeaturesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/country-features")
@RequiredArgsConstructor
@Profile("!test")
public class CountryFeaturesRestApiAdapter implements CountryFeaturesRestApiPort {

    private final CreateCountryFeatureCommand createCountryFeatureCommand;
    private final UpdateCountryFeatureCommand updateCountryFeatureCommand;
    private final GetCountryFeatureByReferenceCommand getCountryFeatureByReferenceCommand;
    private final GetCountryFeatureListCommand getCountryFeatureListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericCountryFeatureApiModel genericCountryFeatureApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateCountryFeatureCommandRequest createCountryFeatureCommandRequest = CreateCountryFeatureCommandRequest.builder()
                .genericCountryFeatureApiModel(genericCountryFeatureApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createCountryFeatureCommand.execute(createCountryFeatureCommandRequest);
    }

    @PutMapping("/{reference}")
    @Override
    public void update(
            @PathVariable String reference,
            @RequestBody GenericCountryFeatureApiModel genericCountryFeatureApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateCountryFeatureCommandRequest updateCountryFeatureCommandRequest = UpdateCountryFeatureCommandRequest.builder()
                .reference(reference)
                .genericCountryFeatureApiModel(genericCountryFeatureApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateCountryFeatureCommand.execute(updateCountryFeatureCommandRequest);
    }

    @GetMapping("/{reference}")
    @ResponseBody
    @Override
    public CountryFeature getByReference(
            @PathVariable String reference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(reference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryFeatureByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<CountryFeature> listCountryFeatures(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCountryFeatureListCommand.execute(genericListCommandRequest);
    }

}
