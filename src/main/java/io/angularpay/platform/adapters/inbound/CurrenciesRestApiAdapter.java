package io.angularpay.platform.adapters.inbound;

import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.domain.commands.currencies.*;
import io.angularpay.platform.models.*;
import io.angularpay.platform.ports.inbound.CurrenciesRestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.platform.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/platform/currencies")
@RequiredArgsConstructor
@Profile("!test")
public class CurrenciesRestApiAdapter implements CurrenciesRestApiPort {

    private final CreateCurrencyCommand createCurrencyCommand;
    private final UpdateCurrencyCommand updateCurrencyCommand;
    private final GetCurrencyByReferenceCommand getCurrencyByReferenceCommand;
    private final EnableCurrencyCommand enableCurrencyCommand;
    private final GetCurrencyByCodeCommand getCurrencyByCodeCommand;
    private final GetCurrencyListCommand getCurrencyListCommand;

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @Override
    public GenericReferenceResponse create(
            @RequestBody GenericCurrencyApiModel genericCurrencyApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        CreateCurrencyCommandRequest createCurrencyCommandRequest = CreateCurrencyCommandRequest.builder()
                .genericCurrencyApiModel(genericCurrencyApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.createCurrencyCommand.execute(createCurrencyCommandRequest);
    }

    @PutMapping("/{currencyReference}")
    @Override
    public void update(
            @PathVariable String currencyReference,
            @RequestBody GenericCurrencyApiModel genericCurrencyApiModel,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        UpdateCurrencyCommandRequest updateCurrencyCommandRequest = UpdateCurrencyCommandRequest.builder()
                .reference(currencyReference)
                .genericCurrencyApiModel(genericCurrencyApiModel)
                .authenticatedUser(authenticatedUser)
                .build();
        this.updateCurrencyCommand.execute(updateCurrencyCommandRequest);
    }

    @GetMapping("/{currencyReference}")
    @ResponseBody
    @Override
    public Currency getByReference(
            @PathVariable String currencyReference,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericReferenceCommandRequest genericReferenceCommandRequest = GenericReferenceCommandRequest.builder()
                .reference(currencyReference)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCurrencyByReferenceCommand.execute(genericReferenceCommandRequest);
    }

    @PutMapping("/{currencyReference}/enable/{enable}")
    @Override
    public void enableCurrency(
            @PathVariable String currencyReference,
            @PathVariable boolean enable,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        EnableCurrencyCommandRequest enableCurrencyCommandRequest = EnableCurrencyCommandRequest.builder()
                .reference(currencyReference)
                .enable(enable)
                .authenticatedUser(authenticatedUser)
                .build();
        this.enableCurrencyCommand.execute(enableCurrencyCommandRequest);
    }

    @GetMapping("/currency-code/{code}")
    @ResponseBody
    @Override
    public Currency getByCode(
            @PathVariable String code,
            @RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetCurrencyByCodeCommandRequest getCurrencyByCodeCommandRequest = GetCurrencyByCodeCommandRequest.builder()
                .code(code)
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCurrencyByCodeCommand.execute(getCurrencyByCodeCommandRequest);
    }

    @GetMapping
    @ResponseBody
    @Override
    public List<Currency> listCurrencies(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GenericListCommandRequest genericListCommandRequest = GenericListCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return this.getCurrencyListCommand.execute(genericListCommandRequest);
    }

}
