package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.Country;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CountryRepository extends MongoRepository<Country, String> {

    Optional<Country> findByReference(String reference);
    Optional<Country> findByCode(String code);
    Optional<Country> findByIsoCode2(String isoCode2);
    Optional<Country> findByIsoCode3(String isoCode3);
}
