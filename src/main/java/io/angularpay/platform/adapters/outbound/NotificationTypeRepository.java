package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.NotificationCode;
import io.angularpay.platform.domain.NotificationType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface NotificationTypeRepository extends MongoRepository<NotificationType, String> {

    Optional<NotificationType> findByReference(String reference);
    Optional<NotificationType> findByCode(NotificationCode code);
}
