package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.OtpCode;
import io.angularpay.platform.domain.OtpType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface OtpTypeRepository extends MongoRepository<OtpType, String> {

    Optional<OtpType> findByReference(String reference);
    Optional<OtpType> findByCode(OtpCode code);
}
