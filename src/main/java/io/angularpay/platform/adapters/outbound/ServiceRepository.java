package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.Service;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ServiceRepository extends MongoRepository<Service, String> {

    Optional<Service> findByReference(String reference);
    Optional<Service> findByCode(String code);
}
