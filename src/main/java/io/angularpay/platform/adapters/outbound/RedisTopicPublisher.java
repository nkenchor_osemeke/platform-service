package io.angularpay.platform.adapters.outbound;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RedisTopicPublisher {

    private final StringRedisTemplate template;

    public void publishUpdates(String topic, String message) {
        template.convertAndSend(topic, message);
    }

}
