package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.domain.Service;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CurrencyRepository extends MongoRepository<Currency, String> {

    Optional<Currency> findByReference(String reference);
    Optional<Currency> findByCode(String code);
}
