package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.TTLConfiguration;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TTLConfigurationRepository extends MongoRepository<TTLConfiguration, String> {

}
