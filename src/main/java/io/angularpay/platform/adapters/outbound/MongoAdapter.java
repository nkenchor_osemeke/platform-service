package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.*;
import io.angularpay.platform.ports.outbound.PersistencePort;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class MongoAdapter implements PersistencePort {

    private final ServiceRepository serviceRepository;
    private final CurrencyRepository currencyRepository;
    private final CountryRepository countryRepository;
    private final NotificationTypeRepository notificationTypeRepository;
    private final OtpTypeRepository otpTypeRepository;
    private final TTLConfigurationRepository ttlConfigurationRepository;
    private final MaturityConfigurationRepository maturityConfigurationRepository;
    private final CountryFeatureRepository countryFeatureRepository;

    @Override
    public Service createService(Service request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return serviceRepository.save(request);
    }

    @Override
    public Service updateService(Service request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return serviceRepository.save(request);
    }

    @Override
    public Optional<Service> findServiceByReference(String reference) {
        return serviceRepository.findByReference(reference);
    }

    @Override
    public Optional<Service> findServiceByCode(String code) {
        return serviceRepository.findByCode(code);
    }

    @Override
    public List<Service> listServices() {
        return serviceRepository.findAll();
    }

    @Override
    public Currency createCurrency(Currency request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return currencyRepository.save(request);
    }

    @Override
    public Currency updateCurrency(Currency request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return currencyRepository.save(request);
    }

    @Override
    public Optional<Currency> findCurrencyByReference(String reference) {
        return currencyRepository.findByReference(reference);
    }

    @Override
    public Optional<Currency> findCurrencyByCode(String code) {
        return currencyRepository.findByCode(code);
    }

    @Override
    public List<Currency> listCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public Country createCountry(Country request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return countryRepository.save(request);
    }

    @Override
    public Country updateCountry(Country request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return countryRepository.save(request);
    }

    @Override
    public Optional<Country> findCountryByReference(String reference) {
        return countryRepository.findByReference(reference);
    }

    @Override
    public Optional<Country> findCountryByCode(String code) {
        return countryRepository.findByCode(code);
    }

    @Override
    public Optional<Country> findCountryByIsoCode2(String isoCode2) {
        return countryRepository.findByIsoCode2(isoCode2);
    }

    @Override
    public Optional<Country> findCountryByIsoCode3(String isoCode3) {
        return countryRepository.findByIsoCode3(isoCode3);
    }

    @Override
    public List<Country> listCountries() {
        return countryRepository.findAll();
    }

   @Override
    public NotificationType createNotificationType(NotificationType request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return notificationTypeRepository.save(request);
    }

    @Override
    public NotificationType updateNotificationType(NotificationType request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return notificationTypeRepository.save(request);
    }

    @Override
    public Optional<NotificationType> findNotificationTypeByReference(String reference) {
        return notificationTypeRepository.findByReference(reference);
    }

    @Override
    public Optional<NotificationType> findNotificationTypeByCode(NotificationCode code) {
        return notificationTypeRepository.findByCode(code);
    }

    @Override
    public List<NotificationType> listNotificationTypes() {
        return notificationTypeRepository.findAll();
    }

    @Override
    public OtpType createOtpType(OtpType request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return otpTypeRepository.save(request);
    }

    @Override
    public OtpType updateOtpType(OtpType request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return otpTypeRepository.save(request);
    }

    @Override
    public Optional<OtpType> findOtpTypeByReference(String reference) {
        return otpTypeRepository.findByReference(reference);
    }

    @Override
    public Optional<OtpType> findOtpTypeByCode(OtpCode code) {
        return otpTypeRepository.findByCode(code);
    }

    @Override
    public List<OtpType> listOtpTypes() {
        return otpTypeRepository.findAll();
    }


    @Override
    public TTLConfiguration createTTLConfiguration(TTLConfiguration request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return ttlConfigurationRepository.save(request);
    }

    @Override
    public TTLConfiguration updateTTLConfiguration(TTLConfiguration request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return ttlConfigurationRepository.save(request);
    }

    @Override
    public Optional<TTLConfiguration> getTTLConfiguration() {
        return ttlConfigurationRepository.findAll().stream().findFirst();
    }

    @Override
    public MaturityConfiguration createMaturityConfiguration(MaturityConfiguration request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return maturityConfigurationRepository.save(request);
    }

    @Override
    public MaturityConfiguration updateMaturityConfiguration(MaturityConfiguration request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return maturityConfigurationRepository.save(request);
    }

    @Override
    public Optional<MaturityConfiguration> findMaturityConfigurationByReference(String reference) {
        return maturityConfigurationRepository.findByReference(reference);
    }

    @Override
    public Optional<MaturityConfiguration> findMaturityConfigurationByServiceCode(String serviceCode) {
        return maturityConfigurationRepository.findByServiceCode(serviceCode);
    }

    @Override
    public List<MaturityConfiguration> listMaturityConfigurations() {
        return maturityConfigurationRepository.findAll();
    }

    @Override
    public CountryFeature createCountryFeature(CountryFeature request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return countryFeatureRepository.save(request);
    }

    @Override
    public CountryFeature updateCountryFeature(CountryFeature request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return countryFeatureRepository.save(request);
    }

    @Override
    public Optional<CountryFeature> findCountryFeatureByReference(String reference) {
        return countryFeatureRepository.findByReference(reference);
    }

    @Override
    public Optional<CountryFeature> findCountryFeatureByCountryReference(String countryReference) {
        return countryFeatureRepository.findByCountryReference(countryReference);
    }

    @Override
    public List<CountryFeature> listCountryFeatures() {
        return countryFeatureRepository.findAll();
    }
}
