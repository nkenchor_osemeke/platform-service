package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.MaturityConfiguration;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MaturityConfigurationRepository extends MongoRepository<MaturityConfiguration, String> {

    Optional<MaturityConfiguration> findByReference(String reference);
    Optional<MaturityConfiguration> findByServiceCode(String serviceCode);
}
