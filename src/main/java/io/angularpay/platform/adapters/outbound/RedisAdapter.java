package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.ports.outbound.OutboundMessagingPort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Profile("!test")
public class RedisAdapter implements OutboundMessagingPort {

    private final RedisHashClient redisHashClient;
    private final RedisTopicPublisher redisTopicPublisher;

    @Override
    public void publishPlatformConfigurations(Map<String, String> platformConfigurations) {
        this.redisHashClient.publishPlatformConfigurations(platformConfigurations);
    }

    @Override
    public void updatePlatformConfigurationHash(String field, String value) {
        this.redisHashClient.updatePlatformConfiguration(field, value);
    }

    @Override
    public void publishPlatformConfigurationUpdate(String topic, String value) {
        this.redisTopicPublisher.publishUpdates(topic, value);
    }

}
