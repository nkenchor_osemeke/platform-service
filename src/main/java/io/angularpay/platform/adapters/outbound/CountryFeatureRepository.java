package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.domain.CountryFeature;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CountryFeatureRepository extends MongoRepository<CountryFeature, String> {

    Optional<CountryFeature> findByReference(String reference);
    Optional<CountryFeature> findByCountryReference(String countryReference);
}
