package io.angularpay.platform.adapters.outbound;

import io.angularpay.platform.configurations.AngularPayConfiguration;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class RedisHashClient {

    private final AngularPayConfiguration configuration;

    private Jedis jedisInstance() {
        return new Jedis(
                configuration.getRedis().getHost(),
                configuration.getRedis().getPort(),
                configuration.getRedis().getTimeout()
        );
    }

    public void publishPlatformConfigurations(Map<String, String> platformConfigurations) {
        try (Jedis jedis = jedisInstance()) {
            jedis.hmset(PlatformConfigurationIdentifier.getHashName(), platformConfigurations);
        }
    }

    public void updatePlatformConfiguration(String field, String value) {
        try (Jedis jedis = jedisInstance()) {
            jedis.hset(PlatformConfigurationIdentifier.getHashName(), field, value);
        }
    }
}
