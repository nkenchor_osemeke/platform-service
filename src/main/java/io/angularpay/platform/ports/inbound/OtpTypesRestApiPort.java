package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.OtpCode;
import io.angularpay.platform.domain.OtpType;
import io.angularpay.platform.models.GenericOtpTypeApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;

import java.util.List;
import java.util.Map;

public interface OtpTypesRestApiPort {

    GenericReferenceResponse create(GenericOtpTypeApiModel genericOtpTypeApiModel, Map<String, String> headers);
    void update(String otpTypeReference, GenericOtpTypeApiModel genericOtpTypeApiModel, Map<String, String> headers);
    OtpType getByReference(String otpTypeReference, Map<String, String> headers);
    void enableOtpType(String otpTypeReference, boolean enable, Map<String, String> headers);
    OtpType getByCode(OtpCode code, Map<String, String> headers);
    List<OtpType> listOtpTypes(Map<String, String> headers);
}
