package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.CountryFeature;
import io.angularpay.platform.models.GenericCountryFeatureApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;

import java.util.List;
import java.util.Map;

public interface CountryFeaturesRestApiPort {

    GenericReferenceResponse create(GenericCountryFeatureApiModel genericCountryFeatureApiModel, Map<String, String> headers);
    void update(String reference, GenericCountryFeatureApiModel genericCountryFeatureApiModel, Map<String, String> headers);
    CountryFeature getByReference(String reference, Map<String, String> headers);
    List<CountryFeature> listCountryFeatures(Map<String, String> headers);
}
