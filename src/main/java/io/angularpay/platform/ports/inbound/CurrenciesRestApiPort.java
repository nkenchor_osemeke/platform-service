package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.models.GenericCurrencyApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;

import java.util.List;
import java.util.Map;

public interface CurrenciesRestApiPort {

    GenericReferenceResponse create(GenericCurrencyApiModel genericCurrencyApiModel, Map<String, String> headers);
    void update(String currencyReference, GenericCurrencyApiModel genericCurrencyApiModel, Map<String, String> headers);
    Currency getByReference(String currencyReference, Map<String, String> headers);
    void enableCurrency(String currencyReference, boolean enable, Map<String, String> headers);
    Currency getByCode(String code, Map<String, String> headers);
    List<Currency> listCurrencies(Map<String, String> headers);
}
