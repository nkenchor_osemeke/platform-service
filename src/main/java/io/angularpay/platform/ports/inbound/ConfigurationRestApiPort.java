package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.MaturityConfiguration;
import io.angularpay.platform.domain.TTLConfiguration;
import io.angularpay.platform.models.GenericMaturityApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;
import io.angularpay.platform.models.GenericTTLApiModel;

import java.util.List;
import java.util.Map;

public interface ConfigurationRestApiPort {

    GenericReferenceResponse createTTLConfiguration(GenericTTLApiModel genericTTLApiModel, Map<String, String> headers);
    void updateTTLConfiguration(GenericTTLApiModel genericTTLApiModel, Map<String, String> headers);
    TTLConfiguration getTTLConfiguration(Map<String, String> headers);

    GenericReferenceResponse createMaturityConfiguration(GenericMaturityApiModel genericMaturityApiModel, Map<String, String> headers);
    void updateMaturityConfiguration(String reference, GenericMaturityApiModel genericMaturityApiModel, Map<String, String> headers);
    MaturityConfiguration getMaturityConfigurationByReference(String reference, Map<String, String> headers);
    List<MaturityConfiguration> listMaturityConfiguration(Map<String, String> headers);
}
