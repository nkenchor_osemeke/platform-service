package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.Service;
import io.angularpay.platform.models.GenericReferenceResponse;
import io.angularpay.platform.models.GenericServiceApiModel;

import java.util.List;
import java.util.Map;

public interface ServicesRestApiPort {

    GenericReferenceResponse create(GenericServiceApiModel genericServiceApiModel, Map<String, String> headers);
    void update(String serviceReference, GenericServiceApiModel genericServiceApiModel, Map<String, String> headers);
    Service getByReference(String serviceReference, Map<String, String> headers);
    void enableService(String serviceReference, boolean enable, Map<String, String> headers);
    Service getByCode(String code, Map<String, String> headers);
    List<Service> listServices(Map<String, String> headers);
}
