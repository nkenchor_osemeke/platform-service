package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.Country;
import io.angularpay.platform.models.GenericCountryApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;

import java.util.List;
import java.util.Map;

public interface CountriesRestApiPort {

    GenericReferenceResponse create(GenericCountryApiModel genericCountryApiModel, Map<String, String> headers);
    void update(String countryReference, GenericCountryApiModel genericCountryApiModel, Map<String, String> headers);
    Country getByReference(String countryReference, Map<String, String> headers);
    void enableCountry(String countryReference, boolean enable, Map<String, String> headers);
    Country getByCode(String code, Map<String, String> headers);
    Country getByIsoCode2(String isoCode2, Map<String, String> headers);
    Country getByIsoCode3(String isoCode3, Map<String, String> headers);
    List<Country> listCountries(Map<String, String> headers);
}
