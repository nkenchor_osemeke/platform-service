package io.angularpay.platform.ports.inbound;

import io.angularpay.platform.domain.NotificationCode;
import io.angularpay.platform.domain.NotificationType;
import io.angularpay.platform.models.GenericNotificationTypeApiModel;
import io.angularpay.platform.models.GenericReferenceResponse;

import java.util.List;
import java.util.Map;

public interface NotificationTypesRestApiPort {

    GenericReferenceResponse create(GenericNotificationTypeApiModel genericNotificationTypeApiModel, Map<String, String> headers);
    void update(String notificationTypeReference, GenericNotificationTypeApiModel genericNotificationTypeApiModel, Map<String, String> headers);
    NotificationType getByReference(String notificationTypeReference, Map<String, String> headers);
    void enableNotificationType(String notificationTypeReference, boolean enable, Map<String, String> headers);
    NotificationType getByCode(NotificationCode code, Map<String, String> headers);
    List<NotificationType> listNotificationTypes(Map<String, String> headers);
}
