package io.angularpay.platform.ports.outbound;

import io.angularpay.platform.domain.*;

import java.util.List;
import java.util.Optional;

public interface PersistencePort {
    Service createService(Service service);
    Service updateService(Service service);
    Optional<Service> findServiceByReference(String reference);
    Optional<Service> findServiceByCode(String name);
    List<Service> listServices();

    Currency createCurrency(Currency currency);
    Currency updateCurrency(Currency currency);
    Optional<Currency> findCurrencyByReference(String reference);
    Optional<Currency> findCurrencyByCode(String code);
    List<Currency> listCurrencies();

    Country createCountry(Country country);
    Country updateCountry(Country country);
    Optional<Country> findCountryByReference(String reference);
    Optional<Country> findCountryByCode(String code);
    Optional<Country> findCountryByIsoCode2(String isoCode2);
    Optional<Country> findCountryByIsoCode3(String isoCode3);
    List<Country> listCountries();

    NotificationType createNotificationType(NotificationType notificationType);
    NotificationType updateNotificationType(NotificationType notificationType);
    Optional<NotificationType> findNotificationTypeByReference(String reference);
    Optional<NotificationType> findNotificationTypeByCode(NotificationCode code);
    List<NotificationType> listNotificationTypes();

    OtpType createOtpType(OtpType otpType);
    OtpType updateOtpType(OtpType otpType);
    Optional<OtpType> findOtpTypeByReference(String reference);
    Optional<OtpType> findOtpTypeByCode(OtpCode code);
    List<OtpType> listOtpTypes();

    TTLConfiguration createTTLConfiguration(TTLConfiguration ttlConfiguration);
    TTLConfiguration updateTTLConfiguration(TTLConfiguration ttlConfiguration);
    Optional<TTLConfiguration> getTTLConfiguration();

    MaturityConfiguration createMaturityConfiguration(MaturityConfiguration maturityConfiguration);
    MaturityConfiguration updateMaturityConfiguration(MaturityConfiguration maturityConfiguration);
    Optional<MaturityConfiguration> findMaturityConfigurationByReference(String reference);
    Optional<MaturityConfiguration> findMaturityConfigurationByServiceCode(String serviceCode);
    List<MaturityConfiguration> listMaturityConfigurations();

    CountryFeature createCountryFeature(CountryFeature countryFeature);
    CountryFeature updateCountryFeature(CountryFeature countryFeature);
    Optional<CountryFeature> findCountryFeatureByReference(String reference);
    Optional<CountryFeature> findCountryFeatureByCountryReference(String countryReference);
    List<CountryFeature> listCountryFeatures();
}
