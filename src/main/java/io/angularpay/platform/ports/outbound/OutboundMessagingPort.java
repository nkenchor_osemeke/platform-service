package io.angularpay.platform.ports.outbound;

import java.util.Map;

public interface OutboundMessagingPort {
    void publishPlatformConfigurations(Map<String, String> platformConfigurations);
    void updatePlatformConfigurationHash(String field, String value);
    void publishPlatformConfigurationUpdate(String topic, String value);
}
