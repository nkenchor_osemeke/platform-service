
package io.angularpay.platform.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("countries")
public class Country {

    @Id
    private String id;
    @Version
    private int version;
    private String reference;
    @JsonProperty("created_on")
    private String createdOn;
    @JsonProperty("last_modified")
    private String lastModified;
    private String name;
    private String code;
    @JsonProperty("dialing_code")
    private String dialingCode;
    @JsonProperty("iso_code_2")
    private String isoCode2;
    @JsonProperty("iso_code_3")
    private String isoCode3;
    private boolean enabled;
}
