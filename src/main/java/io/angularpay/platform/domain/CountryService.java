
package io.angularpay.platform.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryService {

    @NotEmpty
    @JsonProperty("service_reference")
    private String serviceReference;

    @NotEmpty
    @Size(min = 3, max = 3)
    @JsonProperty("service_code")
    private String serviceCode;
}
