package io.angularpay.platform.domain;

public enum OtpCode {
    SEND_AS_SMS_ONLY,
    SEND_AS_EMAIL_ONLY,
    SEND_AS_SMS_AND_EMAIL
}
