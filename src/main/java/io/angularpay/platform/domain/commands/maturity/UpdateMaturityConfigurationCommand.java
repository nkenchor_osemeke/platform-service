package io.angularpay.platform.domain.commands.maturity;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.MaturityConfiguration;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.CommandException;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.UpdateMaturityConfigurationCommandRequest;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.support.CronExpression;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.exceptions.ErrorCode.INVALID_CRON_EXPRESSION;
import static io.angularpay.platform.helpers.CommandHelper.getMaturityConfigurationByReferenceOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class UpdateMaturityConfigurationCommand extends AbstractCommand<UpdateMaturityConfigurationCommandRequest, Void>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public UpdateMaturityConfigurationCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("UpdateMaturityConfigurationCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected Void handle(UpdateMaturityConfigurationCommandRequest request) {
        if (!CronExpression.isValidExpression(request.getGenericMaturityApiModel().getCron())) {
            throw CommandException.builder()
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .errorCode(INVALID_CRON_EXPRESSION)
                    .message(INVALID_CRON_EXPRESSION.getDefaultMessage())
                    .build();
        }

        MaturityConfiguration found = getMaturityConfigurationByReferenceOrThrow(this.mongoAdapter, request.getReference());

        MaturityConfiguration maturityConfiguration = found.toBuilder()
                .cron(request.getGenericMaturityApiModel().getCron())
                .slaDays(request.getGenericMaturityApiModel().getSlaDays())
                .serviceCode(request.getGenericMaturityApiModel().getServiceCode())
                .build();

        this.mongoAdapter.updateMaturityConfiguration(maturityConfiguration);
        return null;
    }

    @Override
    protected List<ErrorObject> validate(UpdateMaturityConfigurationCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_MATURITY_CONFIGURATIONS;
    }
}
