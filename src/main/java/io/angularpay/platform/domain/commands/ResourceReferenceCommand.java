package io.angularpay.platform.domain.commands;

public interface ResourceReferenceCommand<T, R> {

    R map(T referenceResponse);
}
