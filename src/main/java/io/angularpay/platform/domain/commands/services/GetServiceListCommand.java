package io.angularpay.platform.domain.commands.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Service;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericListCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Arrays;
import java.util.List;

@org.springframework.stereotype.Service
public class GetServiceListCommand extends AbstractCommand<GenericListCommandRequest, List<Service>> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetServiceListCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetServiceListCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected List<Service> handle(GenericListCommandRequest request) {
        return this.mongoAdapter.listServices();
    }

    @Override
    protected List<ErrorObject> validate(GenericListCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Arrays.asList(
                Role.ROLE_UNVERIFIED_USER,
                Role.ROLE_VERIFIED_USER,
                Role.ROLE_PLATFORM_ADMIN
        );
    }
}
