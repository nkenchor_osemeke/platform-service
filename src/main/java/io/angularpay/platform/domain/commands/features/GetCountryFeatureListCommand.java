package io.angularpay.platform.domain.commands.features;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.CountryFeature;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericListCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

@org.springframework.stereotype.Service
public class GetCountryFeatureListCommand extends AbstractCommand<GenericListCommandRequest, List<CountryFeature>> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetCountryFeatureListCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetCountryFeatureListCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected List<CountryFeature> handle(GenericListCommandRequest request) {
        return this.mongoAdapter.listCountryFeatures();
    }

    @Override
    protected List<ErrorObject> validate(GenericListCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
