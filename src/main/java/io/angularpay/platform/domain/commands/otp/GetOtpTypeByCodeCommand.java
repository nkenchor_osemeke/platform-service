package io.angularpay.platform.domain.commands.otp;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.OtpType;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GetOtpTypeByCodeCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getOtpTypeByCodeOrThrow;

@org.springframework.stereotype.Service
public class GetOtpTypeByCodeCommand extends AbstractCommand<GetOtpTypeByCodeCommandRequest, OtpType> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetOtpTypeByCodeCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetOtpTypeByCodeCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected OtpType handle(GetOtpTypeByCodeCommandRequest request) {
        return getOtpTypeByCodeOrThrow(this.mongoAdapter, request.getCode());
    }

    @Override
    protected List<ErrorObject> validate(GetOtpTypeByCodeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
