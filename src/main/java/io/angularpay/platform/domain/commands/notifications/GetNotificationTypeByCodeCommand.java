package io.angularpay.platform.domain.commands.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.NotificationType;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GetNotificationTypeByCodeCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getNotificationTypeByCodeOrThrow;

@org.springframework.stereotype.Service
public class GetNotificationTypeByCodeCommand extends AbstractCommand<GetNotificationTypeByCodeCommandRequest, NotificationType> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetNotificationTypeByCodeCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetNotificationTypeByCodeCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected NotificationType handle(GetNotificationTypeByCodeCommandRequest request) {
        return getNotificationTypeByCodeOrThrow(this.mongoAdapter, request.getCode());
    }

    @Override
    protected List<ErrorObject> validate(GetNotificationTypeByCodeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
