package io.angularpay.platform.domain.commands.countries;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Country;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericListCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Arrays;
import java.util.List;

@org.springframework.stereotype.Service
public class GetCountryListCommand extends AbstractCommand<GenericListCommandRequest, List<Country>> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetCountryListCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetCountryListCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected List<Country> handle(GenericListCommandRequest request) {
        return this.mongoAdapter.listCountries();
    }

    @Override
    protected List<ErrorObject> validate(GenericListCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Arrays.asList(
                Role.ROLE_UNVERIFIED_USER,
                Role.ROLE_VERIFIED_USER,
                Role.ROLE_PLATFORM_ADMIN
        );
    }
}
