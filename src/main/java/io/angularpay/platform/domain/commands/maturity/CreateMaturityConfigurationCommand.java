package io.angularpay.platform.domain.commands.maturity;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.MaturityConfiguration;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.CommandException;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.CreateMaturityConfigurationCommandRequest;
import io.angularpay.platform.models.ResourceReferenceResponse;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.support.CronExpression;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.angularpay.platform.exceptions.ErrorCode.INVALID_CRON_EXPRESSION;
import static io.angularpay.platform.helpers.CommandHelper.validateMaturityConfigurationNotExistOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class CreateMaturityConfigurationCommand extends AbstractCommand<CreateMaturityConfigurationCommandRequest, ResourceReferenceResponse>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public CreateMaturityConfigurationCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("CreateMaturityConfigurationCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected ResourceReferenceResponse handle(CreateMaturityConfigurationCommandRequest request) {
        if (!CronExpression.isValidExpression(request.getGenericMaturityApiModel().getCron())) {
            throw CommandException.builder()
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .errorCode(INVALID_CRON_EXPRESSION)
                    .message(INVALID_CRON_EXPRESSION.getDefaultMessage())
                    .build();
        }

        validateMaturityConfigurationNotExistOrThrow(this.mongoAdapter, request.getGenericMaturityApiModel().getServiceCode());

        MaturityConfiguration maturityConfiguration = MaturityConfiguration.builder()
                .reference(UUID.randomUUID().toString())
                .cron(request.getGenericMaturityApiModel().getCron())
                .slaDays(request.getGenericMaturityApiModel().getSlaDays())
                .serviceCode(request.getGenericMaturityApiModel().getServiceCode())
                .build();

        MaturityConfiguration response = this.mongoAdapter.createMaturityConfiguration(maturityConfiguration);
        return new ResourceReferenceResponse(response.getReference());
    }

    @Override
    protected List<ErrorObject> validate(CreateMaturityConfigurationCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_MATURITY_CONFIGURATIONS;
    }
}
