package io.angularpay.platform.domain.commands.countries;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Country;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.CreateCountryCommandRequest;
import io.angularpay.platform.models.ResourceReferenceResponse;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.angularpay.platform.helpers.CommandHelper.validateCountryNotExistOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class CreateCountryCommand extends AbstractCommand<CreateCountryCommandRequest, ResourceReferenceResponse>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public CreateCountryCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("CreateCountryCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected ResourceReferenceResponse handle(CreateCountryCommandRequest request) {
        validateCountryNotExistOrThrow(this.mongoAdapter, request.getGenericCountryApiModel().getCode());

        Country country = Country.builder()
                .reference(UUID.randomUUID().toString())
                .name(request.getGenericCountryApiModel().getName())
                .code(request.getGenericCountryApiModel().getCode())
                .dialingCode(request.getGenericCountryApiModel().getDialingCode())
                .isoCode2(request.getGenericCountryApiModel().getIsoCode2())
                .isoCode3(request.getGenericCountryApiModel().getIsoCode3())
                .enabled(request.getGenericCountryApiModel().isEnabled())
                .build();

        Country response = this.mongoAdapter.createCountry(country);
        return new ResourceReferenceResponse(response.getReference());
    }

    @Override
    protected List<ErrorObject> validate(CreateCountryCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_COUNTRIES;
    }
}
