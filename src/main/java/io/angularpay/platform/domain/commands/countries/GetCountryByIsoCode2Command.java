package io.angularpay.platform.domain.commands.countries;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Country;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GetCountryByCodeCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getCountryByIsoCode2OrThrow;

@org.springframework.stereotype.Service
public class GetCountryByIsoCode2Command extends AbstractCommand<GetCountryByCodeCommandRequest, Country> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetCountryByIsoCode2Command(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetCountryByIsoCode2Command", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected Country handle(GetCountryByCodeCommandRequest request) {
        return getCountryByIsoCode2OrThrow(this.mongoAdapter, request.getCode());
    }

    @Override
    protected List<ErrorObject> validate(GetCountryByCodeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
