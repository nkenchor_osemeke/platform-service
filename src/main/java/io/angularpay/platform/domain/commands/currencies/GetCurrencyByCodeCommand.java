package io.angularpay.platform.domain.commands.currencies;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GetCurrencyByCodeCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getCurrencyByCodeOrThrow;

@org.springframework.stereotype.Service
public class GetCurrencyByCodeCommand extends AbstractCommand<GetCurrencyByCodeCommandRequest, Currency> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetCurrencyByCodeCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetCurrencyByCodeCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected Currency handle(GetCurrencyByCodeCommandRequest request) {
        return getCurrencyByCodeOrThrow(this.mongoAdapter, request.getCode());
    }

    @Override
    protected List<ErrorObject> validate(GetCurrencyByCodeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
