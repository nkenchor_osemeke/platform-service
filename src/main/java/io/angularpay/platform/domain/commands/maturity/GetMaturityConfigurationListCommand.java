package io.angularpay.platform.domain.commands.maturity;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.MaturityConfiguration;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericListCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;

import java.util.Collections;
import java.util.List;

@org.springframework.stereotype.Service
public class GetMaturityConfigurationListCommand extends AbstractCommand<GenericListCommandRequest, List<MaturityConfiguration>> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetMaturityConfigurationListCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetMaturityConfigurationListCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected List<MaturityConfiguration> handle(GenericListCommandRequest request) {
        return this.mongoAdapter.listMaturityConfigurations();
    }

    @Override
    protected List<ErrorObject> validate(GenericListCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
