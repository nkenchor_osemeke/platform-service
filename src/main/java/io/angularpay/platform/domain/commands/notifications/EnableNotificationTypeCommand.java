package io.angularpay.platform.domain.commands.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.NotificationType;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.EnableNotificationTypeCommandRequest;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getNotificationTypeByReferenceOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class EnableNotificationTypeCommand extends AbstractCommand<EnableNotificationTypeCommandRequest, Void>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public EnableNotificationTypeCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("EnableNotificationTypeCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected Void handle(EnableNotificationTypeCommandRequest request) {
        NotificationType found = getNotificationTypeByReferenceOrThrow(this.mongoAdapter, request.getReference());
        found.setEnabled(request.isEnable());
        this.mongoAdapter.updateNotificationType(found);
        return null;
    }

    @Override
    protected List<ErrorObject> validate(EnableNotificationTypeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_NOTIFICATION_TYPES;
    }
}
