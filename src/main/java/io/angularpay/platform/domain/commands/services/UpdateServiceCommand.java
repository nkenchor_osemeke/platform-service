package io.angularpay.platform.domain.commands.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.Service;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.UpdateServiceCommandRequest;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getServiceByReferenceOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class UpdateServiceCommand extends AbstractCommand<UpdateServiceCommandRequest, Void>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public UpdateServiceCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("UpdateServiceCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected Void handle(UpdateServiceCommandRequest request) {
        Service found = getServiceByReferenceOrThrow(this.mongoAdapter, request.getReference());

        Service service = found.toBuilder()
                .name(request.getGenericServiceApiModel().getName())
                .description(request.getGenericServiceApiModel().getDescription())
                .code(request.getGenericServiceApiModel().getCode())
                .type(request.getGenericServiceApiModel().getType())
                .writtenIn(request.getGenericServiceApiModel().getWrittenIn())
                .cipherProtected(request.getGenericServiceApiModel().isCipherProtected())
                .enabled(request.getGenericServiceApiModel().isEnabled())
                .build();

        this.mongoAdapter.updateService(service);
        return null;
    }

    @Override
    protected List<ErrorObject> validate(UpdateServiceCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_SERVICES;
    }
}
