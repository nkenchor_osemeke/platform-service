package io.angularpay.platform.domain.commands.features;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.CountryFeature;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.UpdateCountryFeatureCommandRequest;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getCountryFeatureByReferenceOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class UpdateCountryFeatureCommand extends AbstractCommand<UpdateCountryFeatureCommandRequest, Void>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public UpdateCountryFeatureCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("UpdateCountryFeatureCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected Void handle(UpdateCountryFeatureCommandRequest request) {
        CountryFeature found = getCountryFeatureByReferenceOrThrow(this.mongoAdapter, request.getReference());

        CountryFeature countryFeature = found.toBuilder()
                .countryReference(request.getGenericCountryFeatureApiModel().getCountryReference())
                .countryName(request.getGenericCountryFeatureApiModel().getCountryName())
                .services(request.getGenericCountryFeatureApiModel().getServices())
                .build();

        this.mongoAdapter.updateCountryFeature(countryFeature);
        return null;
    }

    @Override
    protected List<ErrorObject> validate(UpdateCountryFeatureCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_COUNTRY_FEATURES;
    }
}
