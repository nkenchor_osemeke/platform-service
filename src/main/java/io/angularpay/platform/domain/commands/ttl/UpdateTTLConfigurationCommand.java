package io.angularpay.platform.domain.commands.ttl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.TTLConfiguration;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericTTLConfigurationCommandRequest;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.helpers.CommandHelper.getTTLConfigurationOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class UpdateTTLConfigurationCommand extends AbstractCommand<GenericTTLConfigurationCommandRequest, Void>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public UpdateTTLConfigurationCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("UpdateTTLConfigurationCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected Void handle(GenericTTLConfigurationCommandRequest request) {
        TTLConfiguration found = getTTLConfigurationOrThrow(this.mongoAdapter);

        TTLConfiguration ttlConfiguration = found.toBuilder()
                .timeUnit(request.getGenericTTLApiModel().getTimeUnit())
                .value(request.getGenericTTLApiModel().getValue())
                .build();

        this.mongoAdapter.updateTTLConfiguration(ttlConfiguration);
        return null;
    }

    @Override
    protected List<ErrorObject> validate(GenericTTLConfigurationCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_TTL_CONFIGURATION;
    }
}
