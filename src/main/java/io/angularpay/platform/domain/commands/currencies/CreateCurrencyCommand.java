package io.angularpay.platform.domain.commands.currencies;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.CreateCurrencyCommandRequest;
import io.angularpay.platform.models.ResourceReferenceResponse;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.angularpay.platform.helpers.CommandHelper.validateCurrencyNotExistOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class CreateCurrencyCommand extends AbstractCommand<CreateCurrencyCommandRequest, ResourceReferenceResponse>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public CreateCurrencyCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("CreateCurrencyCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected ResourceReferenceResponse handle(CreateCurrencyCommandRequest request) {
        validateCurrencyNotExistOrThrow(this.mongoAdapter, request.getGenericCurrencyApiModel().getCode());

        Currency currency = Currency.builder()
                .reference(UUID.randomUUID().toString())
                .name(request.getGenericCurrencyApiModel().getName())
                .code(request.getGenericCurrencyApiModel().getCode())
                .countryCode(request.getGenericCurrencyApiModel().getCountryCode())
                .enabled(request.getGenericCurrencyApiModel().isEnabled())
                .build();

        Currency response = this.mongoAdapter.createCurrency(currency);
        return new ResourceReferenceResponse(response.getReference());
    }

    @Override
    protected List<ErrorObject> validate(CreateCurrencyCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_CURRENCIES;
    }
}
