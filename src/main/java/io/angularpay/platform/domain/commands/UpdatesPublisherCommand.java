package io.angularpay.platform.domain.commands;

import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;

import java.util.Objects;

public interface UpdatesPublisherCommand {

    PlatformConfigurationsCommand getPlatformConfigurationsCommand();
    PlatformConfigurationIdentifier getPlatformConfigurationIdentifier();

    default void publishUpdates() {
        PlatformConfigurationsCommand command = this.getPlatformConfigurationsCommand();
        PlatformConfigurationIdentifier identifier = this.getPlatformConfigurationIdentifier();
        if (Objects.nonNull(command) && Objects.nonNull(identifier)) {
            command.doPlatformConfigurationUpdate(identifier);
        }
    }
}
