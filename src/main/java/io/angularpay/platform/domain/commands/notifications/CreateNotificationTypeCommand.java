package io.angularpay.platform.domain.commands.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.NotificationType;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.CreateNotificationTypeCommandRequest;
import io.angularpay.platform.models.ResourceReferenceResponse;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.angularpay.platform.helpers.CommandHelper.validateNotificationTypeNotExistOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class CreateNotificationTypeCommand extends AbstractCommand<CreateNotificationTypeCommandRequest, ResourceReferenceResponse>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public CreateNotificationTypeCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("CreateNotificationTypeCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected ResourceReferenceResponse handle(CreateNotificationTypeCommandRequest request) {
        validateNotificationTypeNotExistOrThrow(this.mongoAdapter, request.getGenericNotificationTypeApiModel().getCode());

        NotificationType notificationType = NotificationType.builder()
                .reference(UUID.randomUUID().toString())
                .code(request.getGenericNotificationTypeApiModel().getCode())
                .enabled(request.getGenericNotificationTypeApiModel().isEnabled())
                .build();

        NotificationType response = this.mongoAdapter.createNotificationType(notificationType);
        return new ResourceReferenceResponse(response.getReference());
    }

    @Override
    protected List<ErrorObject> validate(CreateNotificationTypeCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_NOTIFICATION_TYPES;
    }
}
