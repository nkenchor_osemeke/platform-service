package io.angularpay.platform.domain.commands.features;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.CountryFeature;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.domain.commands.PlatformConfigurationsCommand;
import io.angularpay.platform.domain.commands.UpdatesPublisherCommand;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.CreateCountryFeatureCommandRequest;
import io.angularpay.platform.models.ResourceReferenceResponse;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static io.angularpay.platform.helpers.CommandHelper.validateCountryFeatureNotExistOrThrow;

@Slf4j
@org.springframework.stereotype.Service
@Profile("!test")
public class CreateCountryFeatureCommand extends AbstractCommand<CreateCountryFeatureCommandRequest, ResourceReferenceResponse>
        implements UpdatesPublisherCommand {

    private final DefaultConstraintValidator validator;
    private final MongoAdapter mongoAdapter;
    private final PlatformConfigurationsCommand platformConfigurationsCommand;

    public CreateCountryFeatureCommand(
            ObjectMapper mapper,
            DefaultConstraintValidator validator,
            MongoAdapter mongoAdapter,
            PlatformConfigurationsCommand platformConfigurationsCommand) {
        super("CreateCountryFeatureCommand", mapper);
        this.validator = validator;
        this.mongoAdapter = mongoAdapter;
        this.platformConfigurationsCommand = platformConfigurationsCommand;
    }

    @Override
    protected ResourceReferenceResponse handle(CreateCountryFeatureCommandRequest request) {
        validateCountryFeatureNotExistOrThrow(this.mongoAdapter, request.getGenericCountryFeatureApiModel().getCountryReference());

        CountryFeature countryFeature = CountryFeature.builder()
                .reference(UUID.randomUUID().toString())
                .countryReference(request.getGenericCountryFeatureApiModel().getCountryReference())
                .countryName(request.getGenericCountryFeatureApiModel().getCountryName())
                .services(request.getGenericCountryFeatureApiModel().getServices())
                .build();

        CountryFeature response = this.mongoAdapter.createCountryFeature(countryFeature);
        return new ResourceReferenceResponse(response.getReference());
    }

    @Override
    protected List<ErrorObject> validate(CreateCountryFeatureCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }

    @Override
    public PlatformConfigurationsCommand getPlatformConfigurationsCommand() {
        return this.platformConfigurationsCommand;
    }

    @Override
    public PlatformConfigurationIdentifier getPlatformConfigurationIdentifier() {
        return PlatformConfigurationIdentifier.PLATFORM_COUNTRY_FEATURES;
    }
}
