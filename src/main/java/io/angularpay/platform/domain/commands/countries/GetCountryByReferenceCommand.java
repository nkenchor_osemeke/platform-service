package io.angularpay.platform.domain.commands.countries;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Country;
import io.angularpay.platform.domain.Role;
import io.angularpay.platform.domain.commands.AbstractCommand;
import io.angularpay.platform.exceptions.CommandException;
import io.angularpay.platform.exceptions.ErrorObject;
import io.angularpay.platform.models.GenericReferenceCommandRequest;
import io.angularpay.platform.validation.DefaultConstraintValidator;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

import static io.angularpay.platform.exceptions.ErrorCode.REQUEST_NOT_FOUND;

@org.springframework.stereotype.Service
public class GetCountryByReferenceCommand extends AbstractCommand<GenericReferenceCommandRequest, Country> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public GetCountryByReferenceCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("GetCountryByReferenceCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected Country handle(GenericReferenceCommandRequest request) {
        return this.mongoAdapter.findCountryByReference(request.getReference())
                .orElseThrow(() -> CommandException.builder()
                        .status(HttpStatus.NOT_FOUND)
                        .errorCode(REQUEST_NOT_FOUND)
                        .message(REQUEST_NOT_FOUND.getDefaultMessage())
                        .build());
    }

    @Override
    protected List<ErrorObject> validate(GenericReferenceCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}
