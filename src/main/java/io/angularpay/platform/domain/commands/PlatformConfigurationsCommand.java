package io.angularpay.platform.domain.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.platform.adapters.outbound.MongoAdapter;
import io.angularpay.platform.domain.Currency;
import io.angularpay.platform.domain.*;
import io.angularpay.platform.exceptions.CommandException;
import io.angularpay.platform.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.platform.ports.outbound.OutboundMessagingPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

import static io.angularpay.platform.exceptions.ErrorCode.INVALID_MESSAGE_ERROR;
import static io.angularpay.platform.models.platform.PlatformConfigurationIdentifier.PLATFORM_BANKS;

@Slf4j
@Service
@Profile("!test")
public class PlatformConfigurationsCommand {

    private final ObjectMapper mapper;
    private final MongoAdapter mongoAdapter;
    private final OutboundMessagingPort outboundMessagingPort;

    public PlatformConfigurationsCommand(
            ObjectMapper mapper,
            OutboundMessagingPort outboundMessagingPort,
            MongoAdapter mongoAdapter) {
        this.mapper = mapper;
        this.mongoAdapter = mongoAdapter;
        this.outboundMessagingPort = outboundMessagingPort;

        log.info("publishing platform configurations to message hash...");
        Map<String, String> configurations = new HashMap<>();
        log.info("reading platform configurations from data source");
        Arrays.stream(PlatformConfigurationIdentifier.values())
                .filter(x -> x != PLATFORM_BANKS)
                .parallel()
                .forEach(identifier -> {
                    String data = this.getPlatformConfigurations(identifier);
                    if (StringUtils.hasText(data)) {
                        configurations.put(identifier.getHashField(), data);
                    }
                });
        outboundMessagingPort.publishPlatformConfigurations(configurations);
    }

    public void doPlatformConfigurationUpdate(PlatformConfigurationIdentifier identifier) {
        log.info("reading platform configurations from data source");
        String data = this.getPlatformConfigurations(identifier);

        if (StringUtils.hasText(data)) {
            log.info("updating platform configurations to message hash...");
            this.outboundMessagingPort.updatePlatformConfigurationHash(identifier.getHashField(), data);

            log.info("publishing platform configurations to message topic...");
            this.outboundMessagingPort.publishPlatformConfigurationUpdate(identifier.getHashField(), data);
        }
    }

    private String getPlatformConfigurations(PlatformConfigurationIdentifier identifier) {
        try {
            switch (identifier) {
                case PLATFORM_BANKS:
                    // not required
                    return "";
                case PLATFORM_COUNTRIES:
                    List<Country> countries = this.mongoAdapter.listCountries();
                    log.info("read platform configurations: {} {}", identifier.name(), countries.size());
                    return mapper.writeValueAsString(countries);
                case PLATFORM_COUNTRY_FEATURES:
                    List<CountryFeature> countryFeatures = this.mongoAdapter.listCountryFeatures();
                    log.info("read platform configurations: {} {}", identifier.name(), countryFeatures.size());
                    return mapper.writeValueAsString(countryFeatures);
                case PLATFORM_CURRENCIES:
                    List<Currency> currencies = this.mongoAdapter.listCurrencies();
                    log.info("read platform configurations: {} {}", identifier.name(), currencies.size());
                    return mapper.writeValueAsString(currencies);
                case PLATFORM_MATURITY_CONFIGURATIONS:
                    List<MaturityConfiguration> maturityConfigurations = this.mongoAdapter.listMaturityConfigurations();
                    log.info("read platform configurations: {} {}", identifier.name(), maturityConfigurations.size());
                    return mapper.writeValueAsString(maturityConfigurations);
                case PLATFORM_NOTIFICATION_TYPES:
                    List<NotificationType> notificationTypes = this.mongoAdapter.listNotificationTypes();
                    log.info("read platform configurations: {} {}", identifier.name(), notificationTypes.size());
                    return mapper.writeValueAsString(notificationTypes);
                case PLATFORM_OTP_TYPES:
                    List<OtpType> otpTypes = this.mongoAdapter.listOtpTypes();
                    log.info("read platform configurations: {} {}", identifier.name(), otpTypes.size());
                    return mapper.writeValueAsString(otpTypes);
                case PLATFORM_SERVICES:
                    List<io.angularpay.platform.domain.Service> services = this.mongoAdapter.listServices();
                    log.info("read platform configurations: {} {}", identifier.name(), services.size());
                    return mapper.writeValueAsString(services);
                case PLATFORM_TTL_CONFIGURATION:
                    Optional<TTLConfiguration> ttlConfiguration = this.mongoAdapter.getTTLConfiguration();
                    log.info("read platform configurations: {}", identifier.name());
                    if (ttlConfiguration.isPresent()) {
                        return mapper.writeValueAsString(ttlConfiguration.get());
                    } else {
                        return "";
                    }
            }
        } catch (Exception exception) {
            log.error("An error occurred while reading platform configurations {} value", identifier.name(), exception);
            throw new RuntimeException(CommandException.builder()
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .errorCode(INVALID_MESSAGE_ERROR)
                    .message(exception.getMessage())
                    .cause(exception)
                    .build());
        }
        return "";
    }

}
