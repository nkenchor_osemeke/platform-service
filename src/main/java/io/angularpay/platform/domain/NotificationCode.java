package io.angularpay.platform.domain;

public enum NotificationCode {
    RING_AND_VIBRATE,
    RING_ONLY,
    VIBRATE_ONLY
}
