package io.angularpay.platform.domain;

public enum ServiceType {
    FEATURE_SERVICE, PLATFORM_SERVICE
}
