package io.angularpay.platform.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@SuperBuilder
public class GenericListCommandRequest extends AccessControl {

    GenericListCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
