package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.angularpay.platform.domain.CountryService;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class GenericCountryFeatureApiModel {

    @NotEmpty
    @JsonProperty("country_reference")
    private String countryReference;

    @NotEmpty
    @JsonProperty("country_name")
    private String countryName;

    @NotEmpty
    @Valid
    private List<CountryService> services;
}
