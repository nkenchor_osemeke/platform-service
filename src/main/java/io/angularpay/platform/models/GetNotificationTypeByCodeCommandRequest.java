package io.angularpay.platform.models;

import io.angularpay.platform.domain.NotificationCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class GetNotificationTypeByCodeCommandRequest extends AccessControl {

    @NotNull
    private NotificationCode code;

    GetNotificationTypeByCodeCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
