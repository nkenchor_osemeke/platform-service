package io.angularpay.platform.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EnableOtpTypeCommandRequest extends AccessControl {

    @NotEmpty
    private String reference;

    private boolean enable;

    EnableOtpTypeCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
