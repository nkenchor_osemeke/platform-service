package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class GenericMaturityApiModel {

    @NotEmpty
    private String cron;

    @JsonProperty("sla_days")
    private int slaDays;

    @NotEmpty
    @Size(min = 3, max = 3)
    @JsonProperty("service_code")
    private String serviceCode;
}
