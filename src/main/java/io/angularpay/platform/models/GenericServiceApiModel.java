package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.angularpay.platform.domain.ServiceType;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GenericServiceApiModel {

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @NotEmpty
    @Size(min = 3, max = 3)
    private String code;

    @NotNull
    private ServiceType type;

    @NotEmpty
    @JsonProperty("written_in")
    private String writtenIn;

    @JsonProperty("cipher_protected")
    private boolean cipherProtected;

    private boolean enabled;
}
