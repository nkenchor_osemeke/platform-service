package io.angularpay.platform.models;

import io.angularpay.platform.domain.NotificationCode;
import io.angularpay.platform.domain.OtpCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class GetOtpTypeByCodeCommandRequest extends AccessControl {

    @NotNull
    private OtpCode code;

    GetOtpTypeByCodeCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
