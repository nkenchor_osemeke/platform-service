package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Data
public class GenericTTLApiModel {

    @NotNull
    @JsonProperty("time_unit")
    private TimeUnit timeUnit;

    private long value;
}
