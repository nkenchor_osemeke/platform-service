package io.angularpay.platform.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@SuperBuilder
public class GenericTTLCommandRequest extends AccessControl {

    GenericTTLCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
