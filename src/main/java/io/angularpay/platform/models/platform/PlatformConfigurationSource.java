
package io.angularpay.platform.models.platform;

import lombok.Getter;

@Getter
public enum PlatformConfigurationSource {
    HASH, TOPIC
}
