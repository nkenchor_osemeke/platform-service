package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class GenericCountryApiModel {

    @NotEmpty
    private String name;

    @NotEmpty
    @Size(min = 3, max = 3)
    private String code;

    @NotEmpty
    @Size(min = 1, max = 7)
    @JsonProperty("dialing_code")
    private String dialingCode;

    @NotEmpty
    @Size(min = 2, max = 2)
    @JsonProperty("iso_code_2")
    private String isoCode2;

    @NotEmpty
    @Size(min = 3, max = 3)
    @JsonProperty("iso_code_3")
    private String isoCode3;

    private boolean enabled;
}
