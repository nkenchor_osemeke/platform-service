package io.angularpay.platform.models;

import io.angularpay.platform.domain.NotificationCode;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GenericNotificationTypeApiModel {

    @NotNull
    private NotificationCode code;

    private boolean enabled;
}
