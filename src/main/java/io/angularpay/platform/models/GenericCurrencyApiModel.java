package io.angularpay.platform.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.angularpay.platform.domain.ServiceType;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GenericCurrencyApiModel {

    @NotEmpty
    private String name;

    @NotEmpty
    @Size(min = 3, max = 3)
    private String code;

    @NotEmpty
    @JsonProperty("country_code")
    private String countryCode;

    private boolean enabled;
}
