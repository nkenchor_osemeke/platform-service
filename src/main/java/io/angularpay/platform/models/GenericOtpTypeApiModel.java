package io.angularpay.platform.models;

import io.angularpay.platform.domain.OtpCode;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GenericOtpTypeApiModel {

    @NotNull
    private OtpCode code;

    private boolean enabled;
}
