package io.angularpay.platform.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class GetCountryByCodeCommandRequest extends AccessControl {

    @NotEmpty
    private String code;

    GetCountryByCodeCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
