package io.angularpay.platform.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EnableCurrencyCommandRequest extends AccessControl {

    @NotEmpty
    private String reference;

    private boolean enable;

    EnableCurrencyCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
