package io.angularpay.platform.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class CreateCountryFeatureCommandRequest extends AccessControl {

    @NotNull
    @Valid
    private GenericCountryFeatureApiModel genericCountryFeatureApiModel;

    CreateCountryFeatureCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}
